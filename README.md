# Node Video Renderer Proof-of-Concept

Most samples in the incredible web tell you to write png files and then pipe them to ffmpeg as frames. This has the problem of having possibly thousands of temporary files.

This is simple sample application how to use node-canvas to draw frames and then stream those in-memory (node-canvas outputs png snapshots) to ffmpeg for video rendering and then the output is streamed to a mp4 file (with macOS compatible settings).

Mix in with canvas libraries such as PIXI.js for easier graphics. Pair with web server code supporting video streaming and you got video casting of real-time rendered video.