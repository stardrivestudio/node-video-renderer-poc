import * as fs from 'fs';
import { Canvas, createCanvas, CanvasRenderingContext2D } from 'canvas';
import { Writable, Readable, PassThrough } from 'stream';
import { FfmpegCommand } from 'fluent-ffmpeg'; 

const writeCanvasToFile = (canvas: Canvas) => {
    const filename = __dirname + '/test.png';
    const out = fs.createWriteStream(filename);
    const stream = canvas.createPNGStream();
    stream.pipe(out);
    out.on('finish', () => {
        console.log(`Wrote ${filename}`);
    });
}

const writeCanvasToStream = async (canvas: Canvas, input: Writable, index: number) => {
    return new Promise((done) => {
        const stream = canvas.createPNGStream();
        stream.pipe(input, { end: false });
        stream.on('end', () => {
            //console.log(`Wrote frame ${index}`);
            stream.unpipe(input);
            done();
        });    
    });
}

interface Video {
    input: Writable;
    output: Readable;
    width: number;
    height: number;
}

const openVideo = (width: number, height: number): Video => {
    const stream = new PassThrough({});
    return { input: stream, output: stream, width, height };
}

const closeVideo = async (video: Video) => {
    video.input.end();
    return new Promise(done => video.input.on('finish', done));
}

class Surface {
    private context2d: CanvasRenderingContext2D;
    constructor(public readonly canvas: Canvas, public readonly width: number, public readonly height: number) {     
        this.context2d = this.canvas.getContext('2d');   
    }
    get context(): CanvasRenderingContext2D {
        return this.context2d;
    }
}

const createDrawSurface = (width: number, height: number) => {
    const canvas = createCanvas(width, height);
    return new Surface(canvas, width, height);
}

const clearSurface = (surface: Surface) => {
    surface.context.clearRect(0, 0, surface.width, surface.height);
}

const drawBox = (surface: Surface, x: number, y: number) => {
    const r = Math.round(Math.random() * 255);
    const g = Math.round(Math.random() * 255);
    const b = Math.round(Math.random() * 255);
    surface.context.fillStyle = `rgb(${r},${g},${b})`;
    surface.context.fillRect(x, y, 20, 20);
}

const drawRandomBox = (surface: Surface) => {
    drawBox(surface, Math.random() * 100, Math.random() * 100);
}

const writeVideoFrames = async () => {
    const width = 100;
    const height = 100;
    const surface = createDrawSurface(width, height);

    const video = openVideo(width, height);

    const videoWriter = await createVideoWriter(video);
    
    // Setup output to go to file
    //const fileOut = fs.createWriteStream('./test.png');
    //video.output.pipe(fileOut);

    // Write and close open ended output
    for (let i = 0; i < 1200; i++) {
        clearSurface(surface);
        drawRandomBox(surface);
        await writeCanvasToStream(surface.canvas, video.input, i);
    }
    await closeVideo(video);
}

const createVideoWriter = async (video: Video): Promise<FfmpegCommand> => {
    const FfmpegCommand = require('fluent-ffmpeg');

    const fps = 30;

    const runner = new Promise<FfmpegCommand>((done) => {
        const ffmpeg = new FfmpegCommand() as FfmpegCommand;
        ffmpeg
            .input(video.output)
            .inputFormat('image2pipe')
            .inputOptions(`-framerate ${fps}`) // input fps
            .noAudio()
            .output('./test.mp4')
            .outputOption('-vf format=yuv420p') // required for QuickTime
            .size(`${video.width}x${video.height}`)
            //.videoBitrate(1000)
            .fps(fps)
            .videoCodec('libx264')
            .on('end', function() {
                console.log('Processing ended.');
            })
            .on('start', function(cmd: string) {
                console.log('Started ffmpeg: ' + cmd);
                done(ffmpeg);
            })
            .on('progress', function(progress: any) {
                console.log('Processing: ' + progress.frames + ' frames done');
            })
            .on('error', function(err: any, stdout: any, stderr: any) {
                console.log('Cannot process video: ' + err.message);
            })
            .run();
    });
    return runner;
}

const main = async () => {
    await writeVideoFrames();
}

// Execute main loop
main();
